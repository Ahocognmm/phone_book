module.exports = function (app) {
    var todoList = require('../controller/ListController');

    app.route('/get')
        .get(todoList.list_all_tasks);

    app.route('/add').
        put(todoList.put_a_user);
};