var express = require('express'),
    app = express(),
    port = process.env.PORT || 3000,
    mysql = require('mysql'),
    pool = require('./api/models/database'),
    bodyParser= require('body-parser');


var routes = require('./api/routes/User');
routes(app);
app.use(bodyParser.urlencoded({extended : true}))
app.use(bodyParser.json());
app.listen(port);

console.log('todo list RESTful API server started on:' + port);