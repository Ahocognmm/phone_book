/**
 * @flow
 */
import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, FlatList, Button, Touchable, ToastAndroid } from 'react-native';
import { StackNavigator } from 'react-navigation';
import SQLite from 'react-native-sqlite-storage';
var db = SQLite.openDatabase({ name: 'test.db', createFromLocation: '~phone_book.db' })
// const sql = require('sqlite3').verbose();

export default class ListPhone extends Component {
    static navigationOptions =
        {
            title: 'List Phone',
        };
    constructor(props) {
        super(props);
        this.state = {
            dataSource: [ {
                "name": "hehe",
                "phone": 1230
            }],
            //     dataSource: ds.cloneWithRows([
                    // {
                    //     "name": "hehe",
                    //     "phone": 1230
                    // },
            //         {
            //             "name": "hehehe",
            //             "phone": 1234
            //         },
            //         {
            //             "name": "he",
            //             "phone": 1235
            //         },
            //         {
            //             "name": "hehehehe",
            //             "phone": 1236
            //         },
            // {
            //     "name": "hehehehehe",
            //     "phone": 1237
            // },
            // {
            //     "name": "hihi",
            //     "phone": 1238
            // },
            // {
            //     "name": "hihihi",
            //     "phone": 1239
            // }
            //     ]),
        };
        ToastAndroid.show('Hello!!', ToastAndroid.SHORT);
    }

    componentDidMount() {
        ToastAndroid.show('hello clgt', ToastAndroid.SHORT);
        db.transaction((tx) => {
            tx.executeSql('SELECT * FROM user', [], (tx, results) => {
                ToastAndroid.show('hello im here', ToastAndroid.SHORT);
                // var len = results.rows.length;
                // if (len > 0) {
                //     var row = results.rows.item(0)
                //     ToastAndroid.show(row.name, ToastAndroid.LONG)
                //     this.setState = { 
                //         dataSource: [{ name: row.name, phone: row.phone_number }] 
                //     };
                // }
                var len = results.rows.length;
                for (let i = 0; i < len; i++) {
                    let row = results.rows.item(i);
                    ToastAndroid.show(row.name +"\t"+ row.phone_number, ToastAndroid.LONG)
                    this.setState((prevState)=>{
                        ({
                            dataSource : prevState.dataSource.push(
                                {"name": row.name, "phone": row.phone_number})
                        })
                    })
                }
            });
        });
    }

    renderSeparator(sectionID, rowID, adjacentRowHighlighted) {
        return (
            <View
                key={`${sectionID}-${rowID}`}
                style={{
                    height: adjacentRowHighlighted ? 4 : 1,
                    backgroundColor: adjacentRowHighlighted ? '#3B5998' : '#CCCCCC',
                }}
            />
        );
    }

    render() {
        return (
            <View>
                <FlatList
                    data={this.state.dataSource}
                    renderItem={({ item }) =>
                        <View style={styles.row}>
                            <Text button onPress={() => this.props.navigation.navigate('Detail', item)} style={styles.text}>
                                {item.name}
                            </Text>
                        </View>
                    }
                    renderSeparator={this.renderSeparator}
                />
                <Button onPress={() => {
                    this.props.navigation.navigate('Detail')
                }} title='Click here babe' />
            </View>
        );
    }

}

const styles = StyleSheet.create({
    row: {
        flexDirection: 'row',
        justifyContent: 'center',
        padding: 10,
        backgroundColor: '#F6F6F6',
    },

    text: {
        flex: 1,
        padding: 10
    }
});
