import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, ListView } from 'react-native';
import { StackNavigator } from 'react-navigation';

export default class Detail extends React.Component {
  static navigationOptions =
    {
      title: 'SecondActivity',
    };


  render() {
    const { navigation } = this.props;
    const name = navigation.getParam('name', 'no-name');
    const phone = navigation.getParam('phone', '00000')
    return (
      <View>

        <Text> {name} </Text>
        <Text> {phone}</Text>

      </View>
    );
  }
}