/** @format */

import {AppRegistry} from 'react-native';
import {name as appName} from './app.json';
import ListPhone from "./ListPhone";
import Detail from "./Detail";
import { createStackNavigator } from 'react-navigation';

const Appli = createStackNavigator({
    Home: { screen: ListPhone },
    Detail: { screen: Detail}
})
export default Appli

AppRegistry.registerComponent(appName, () => Appli);